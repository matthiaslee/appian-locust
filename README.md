## The appian_locust repo

A load testing framework for Appian built on top of locust. With it you should be able to
* Test many different types of SAIL interactions
* Simulate usage within sites and tempo environments
* Move through forms in an API driven manner
* Interact with system webapis

### To view our relevant documentation, visit the docs page
