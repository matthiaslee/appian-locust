*********************************************************************
Welcome to the Appian Performance Library documentation!
*********************************************************************

.. sidebar:: More information

    * `View/contribute on gitlab <https://gitlab.com/appian-oss/appian-locust>`_

Start Here!
-----------

.. toctree::
    :maxdepth: 2

    what_is_appian_locust
    quick_start

Using Locust
------------

.. toctree::
    :maxdepth: 2

    installation
    basic_usage
    how_to_run_locust
    appian_locust_records_example

Advanced Usage
---------------

.. toctree::
    :maxdepth: 2

    debugging
    contributing
    advanced_examples
    limitations

API
-----------

.. toctree::
    :maxdepth: 4

    appian_locust

Appian-Locust Logger
---------------------
.. toctree::
    :maxdepth: 4

    appian_locust.logger

Appian-Locust LoadDriver Utilities
-----------------------------------
.. toctree::
    :maxdepth: 4

appian_locust.loadDriverUtils

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
